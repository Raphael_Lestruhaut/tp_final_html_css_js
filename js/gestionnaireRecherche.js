import DaoJeu from "./DAO/dao_jeux";
import GestionnaireAffichage from "./gestionnaireAffichage.js"

export default class GestionnaireRecherche
{
	static async clickBoutonRecherche()
	{
		const section = document.querySelector("section")
		section.innerHTML = `
		<div class="loader">
		</div>`

		const valeur = document.querySelector(".input")
		const reponse = await DaoJeu.recherche(valeur.value)
		section.innerHTML = ""

		GestionnaireAffichage.afficherListeJeu(reponse, false)
	}
}