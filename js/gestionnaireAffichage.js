import GestionnaireFavoris from "./gestionnaireFavoris.js"

export default class GestionnaireAffichage
{
	static afficherListeJeu(liste, pageFavoris)
	{
		const section = document.querySelector("section")
		section.innerHTML = ""

		const boutonRetour = document.querySelector(".boutonRetour")
		boutonRetour.style.display = "none"

		liste.forEach(jeu =>
		{
			const divJeu = document.createElement('div')
			divJeu.classList.add("divJeu")
			
			//ajout image
			divJeu.innerHTML = `<img src="`+ jeu.screen +`" alt="image du jeu" class="imageJeu"/>`;

			//ajout plateforme
			const divPlateformes = document.createElement('div')
			divPlateformes.classList.add("divPlateformes")
			for (var i = 0; i < 4 && i < jeu.plateforme.length; i++) 
			{
				if(jeu.plateforme[i] === undefined)
				{
					divPlateformes.innerHTML = divPlateformes.innerHTML + "<p>pas d'icone</p>"
				}
				else
				{
					divPlateformes.innerHTML = divPlateformes.innerHTML +`<img src="`+ jeu.plateforme[i].icone +`" alt="image du jeu" class="imagePlateforme"/>`
				}
			}
			if(jeu.plateforme.length > 4)
			{
				const divPlus = document.createElement('div')
				divPlus.classList.add("imagePlateforme")
				divPlus.classList.add("divGeneral")
				const nombre = jeu.plateforme.length - 4
				divPlus.innerHTML = `<p class="pAutre">+`+ nombre +` autres</p>`
				divPlateformes.append(divPlus)
			}
			divJeu.append(divPlateformes)

			//titre du jeu
			const divNom = document.createElement('div')
			divNom.classList.add("divGeneral")
			divNom.classList.add("divNom")
			divNom.innerHTML = `<p>` + jeu.nom + "</p>"
			divJeu.append(divNom)

			divJeu.onclick = () =>
			{
				GestionnaireAffichage.afficheDetail(jeu, liste, pageFavoris)
			}

			section.append(divJeu)
		})
	}

	static afficheDetail(jeu,liste,pageFavoris)
	{
		const section = document.querySelector("section")
		section.innerHTML = ""

		const divPremierLigne = document.createElement('div')
		divPremierLigne.classList.add("divGeneralDetail")

		const divNom = document.createElement('div')
		divNom.classList.add("divGeneralDetail")

		divNom.innerHTML = `
		<div class="divDetailNom">
			<p>` + jeu.nom + `</p><br/>
			<button type="button" class="bouton" id="supprimerFavoris">Supprimer aux favoris</button>
			<button type="button" class="bouton" id="ajoutFavoris">Ajout aux favoris</button>
		</div>`

		divPremierLigne.append(divNom)

		divPremierLigne.innerHTML = divPremierLigne.innerHTML + `<img src="`+ jeu.image +`" alt="image du jeu" class="imgJeuDetail"/>`

		const divSecondLigne = document.createElement('div')
		divSecondLigne.classList.add("divGeneralDetail")
		
		//ajout plateforme
		const divPlateformes = document.createElement('div')
		divPlateformes.classList.add("divPlateformesDetail")
		for (var i = 0; i < 4 && i < jeu.plateforme.length; i++) 
		{
			if(jeu.plateforme[i] === undefined)
			{
				divPlateformes.innerHTML = divPlateformes.innerHTML + "<p>pas d'icone</p>"
			}
			else
			{
				divPlateformes.innerHTML = divPlateformes.innerHTML +`<img src="`+ jeu.plateforme[i].icone +`" alt="image du jeu" class="imagePlateformeDetail"/>`
			}
		}
		if(jeu.plateforme.length > 4)
		{
			const divPlus = document.createElement('div')
			divPlus.classList.add("imagePlateformeDetail")
			divPlus.classList.add("divPlusDetail")
			const nombre = jeu.plateforme.length - 4
			divPlus.innerHTML = `<p class="pAutre">+`+ nombre +` autres</p>`
			divPlateformes.append(divPlus)
		}

		//date de sortie
		const divSortie = document.createElement('div')
		divSortie.classList.add("divSortie")
		if(jeu.sortie == undefined || jeu.sortie)
		{
			divSortie.innerHTML = `<p>Sortie: <br>`+ jeu.date + `</p>`
		}
		else
		{
			divSortie.innerHTML = `<p>Sortie prévu: <br>`+ jeu.date + `</p>`
		}

		divSecondLigne.append(divPlateformes)

		divSecondLigne.append(divSortie)

		//les descriptions
		const divDescriptionCourte = document.createElement('div')
		divDescriptionCourte.classList.add("divGeneralDetail")
		divDescriptionCourte.innerHTML = `<p>` + jeu.description_courte + `</p>`

		const divDescriptionLongue = document.createElement('div')
		divDescriptionLongue.classList.add("divGeneralDetail")
		divDescriptionLongue.classList.add("divDescriptionLongue")
		if(jeu.description_longue != null)
		{
			divDescriptionLongue.innerHTML = `<p>` + jeu.description_longue + `</p>`
		}

		section.append(divPremierLigne)
		section.append(divSecondLigne)
		section.append(divDescriptionCourte)
		section.append(divDescriptionLongue)

		//une fois que tout est append au doc je peux cacher le bouton inutile
		console.log(GestionnaireFavoris.listeJeuFavoris)
		if(GestionnaireFavoris.listeJeuFavoris.findIndex((element) => {
			return element.guid == jeu.guid
		}) >= 0)
		{
			const ajoutFavoris = document.getElementById("ajoutFavoris")
			ajoutFavoris.style.display = "none"
		}
		else
		{
			const supprimerFavoris = document.getElementById("supprimerFavoris")
			supprimerFavoris.style.display = "none"
		}

		//ajout fonction au bouton favoris
		const boutonAjoutFavoris = document.getElementById("ajoutFavoris");
		const boutonSupprimerFavoris = document.getElementById("supprimerFavoris")
		
		boutonAjoutFavoris.onclick = () =>
		{
			GestionnaireFavoris.ajoutFavoris(jeu)
			boutonAjoutFavoris.style.display = "none"
			boutonSupprimerFavoris.style.display = "inline-block"
		}

		boutonSupprimerFavoris.onclick = () =>
		{
			//On gère l'affichage des boutons dans la fonction qui s'execute quand on comfirme la popup
			GestionnaireFavoris.supprimerFavoris(jeu)
		}

		const boutonRetour = document.querySelector(".boutonRetour")
		boutonRetour.style.display = "inline-block"
		boutonRetour.onclick = () =>
		{
			if(pageFavoris)
			{
				GestionnaireAffichage.afficherListeJeu(GestionnaireFavoris.getFavoris(), true)
			}
			else
			{
				GestionnaireAffichage.afficherListeJeu(liste, false)
			}
		}
	}
}