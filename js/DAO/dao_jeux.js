import Jeu from "../Model/jeu.js"
import DaoPlateforme from "./dao_plateforme.js";

export default class DaoJeu
{
	static async recherche(recherche)
	{
		const reponse = await fetch("https://www.giantbomb.com/api/games/?api_key=011c407b28eea9eca47d954c1240f25e21c0fd33&format=json&filter=name:"+recherche)
		
		const data = await reponse.json()

		//récupération plateforme
		const listePlateforme = await DaoPlateforme.recuperationPlateforme()
				
		const resultat = data["results"]
		const listeJeu = []
		resultat.forEach(elementJeu =>
		{
			const jeu = new Jeu(elementJeu)

			//ajout des plateforme au jeu
			const listePlateformeJeu = []
			if(elementJeu.platforms != undefined)
			{
				elementJeu.platforms.forEach(element =>
				{
					listePlateformeJeu.push(listePlateforme[element.id-1])
				})
			}
			jeu.plateforme = listePlateformeJeu
			listeJeu.push(jeu)
			
			
		})
		return listeJeu
	}
}