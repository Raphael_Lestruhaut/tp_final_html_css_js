import Plateforme from "../Model/plateforme.js"

export default class DaoPlateforme
{
	static async recuperationPlateforme()
	{
		const listePlateforme = []

		const reponse = await fetch("https://www.giantbomb.com/api/platforms/?api_key=011c407b28eea9eca47d954c1240f25e21c0fd33&format=json")

		const json = await reponse.json()

		json.results.forEach(element=>
		{
			listePlateforme.push(new Plateforme(element.id, element.image["icon_url"]))
		})

		let resultat = json.number_of_page_results
		
		let offset = 100
		
		while(resultat == 100)
		{
			console.log("https://www.giantbomb.com/api/platforms/?api_key=011c407b28eea9eca47d954c1240f25e21c0fd33&format=json&offset="+offset)
			const reponse = await fetch("https://www.giantbomb.com/api/platforms/?api_key=011c407b28eea9eca47d954c1240f25e21c0fd33&format=json&offset="+offset)

			const json = await reponse.json()

			json.results.forEach(element=>
			{
				listePlateforme.push(new Plateforme(element.id, element.image["icon_url"]))
			})

			resultat = json.number_of_page_results
			offset = offset + 100
		}

		return listePlateforme
	}
}