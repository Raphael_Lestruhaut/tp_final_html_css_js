import GestionnairePopup from "./gestionnairePopup.js"
import GestionnaireAffichage from "./gestionnaireAffichage.js"

export default class GestionnaireFavoris
{
	static listeJeuFavoris = []

	static ajoutFavoris(jeu)
	{
		this.listeJeuFavoris = JSON.parse(localStorage.getItem("favoris"))
		if(this.listeJeuFavoris == null)
		{
			this.listeJeuFavoris = []
		}
		else
		{
			this.listeJeuFavoris = JSON.parse(localStorage.getItem("favoris"))
		}
		this.listeJeuFavoris.push(jeu)
		localStorage.setItem("favoris", JSON.stringify(this.listeJeuFavoris))

		GestionnairePopup.affichePopupInformation(`L'élément à bien été ajouté à la liste des favoris`)
	}

	static supprimerFavoris(jeu)
	{
		this.listeJeuFavoris = JSON.parse(localStorage.getItem("favoris"))
		GestionnairePopup.affichePopupConfirmation("Voulez vraiment supprimer le jeu " + jeu.nom + " de vos favoris", () => {
			const indexJeu = GestionnaireFavoris.listeJeuFavoris.findIndex((element) => {
				return element.guid == jeu.guid
			})

			this.listeJeuFavoris.splice(indexJeu, 1)
			localStorage.setItem("favoris", JSON.stringify(this.listeJeuFavoris))
			GestionnairePopup.affichePopupInformation("Le jeu " + jeu.nom + " a bien été retirer de vos favoris")
			
			//gestion affichage des boutons de favoris
			const boutonAjoutFavoris = document.getElementById("ajoutFavoris");
			const boutonSupprimerFavoris = document.getElementById("supprimerFavoris")
			boutonSupprimerFavoris.style.display = "none"
			boutonAjoutFavoris.style.display = "inline-block"
		})
	}

	static afficherFavoris()
	{
		if(this.listeJeuFavoris == null)
		{
			this.listeJeuFavoris = []
		}
		else
		{
			this.listeJeuFavoris = JSON.parse(localStorage.getItem("favoris"))
		}
		GestionnaireAffichage.afficherListeJeu(this.listeJeuFavoris, true)
	}

	static getFavoris()
	{
		this.listeJeuFavoris = JSON.parse(localStorage.getItem("favoris"))
		if(this.listeJeuFavoris == null)
		{
			return []
		}
		else
		{
			return JSON.parse(localStorage.getItem("favoris"))
		}
	}
}