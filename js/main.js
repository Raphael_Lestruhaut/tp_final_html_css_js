import DaoJeu from "./DAO/dao_jeux.js";
import GestionnaireRecherche from "./gestionnaireRecherche.js"
import GestionnaireFavoris from "./gestionnaireFavoris.js"

export default class App {

	static init = ()=>{
		//localStorage.clear()
		const boutonRecherche = document.getElementById("boutonRecherche");
		if(!boutonRecherche)
		{
			throw new Error("Bouton recherche absent")
		}
		boutonRecherche.addEventListener("click", GestionnaireRecherche.clickBoutonRecherche.bind(GestionnaireRecherche))

		const boutonFavoris = document.getElementById("favoris")
		if(!boutonFavoris)
		{
			throw new Error("Bouton favoris absent")
		}
		boutonFavoris.addEventListener("click", GestionnaireFavoris.afficherFavoris.bind(GestionnaireFavoris))
	}
}

window.onload = App.init;