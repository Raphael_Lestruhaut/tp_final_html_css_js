export default class Jeu
{
	guid = ""

	screen = "";
	plateforme = [];
	nom = "";

	image = "";
	date = "";
	sortie = undefined;
	description_courte = "";
	description_longue = "";

	constructor(jeu)
	{
		this.guid = jeu.guid
		this.screen = jeu.image["screen_url"]
		this.nom = jeu.name
		this.image = jeu.image["small_url"]
		if(jeu.original_release_date == undefined && jeu.expected_release_year == undefined)
		{
			this.date = "inconnu"
		}
		else if(jeu.original_release_date == undefined)
		{
			this.date = jeu.expected_release_year
			this.sortie = false;
		}
		else
		{
			this.date = jeu.original_release_date
			this.sortie = true;
		}
		this.description_courte = jeu.deck
		this.description_longue = jeu.description
	}
}