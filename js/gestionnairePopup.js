export default class GestionnairePopup
{
	static affichePopupInformation(texte)
	{
		
		const popup = document.createElement('div')
		popup.classList.add("popup")
		popup.innerHTML = `
		<div class="popupContent">
			<div class="elementPopup titrePopup">
				<p>Information</p>
			</div>
			<div class="elementPopup">
				<p>` + texte + `</p>
			</div>
		</div>`
		const html = document.querySelector("html")
		html.append(popup)
		popup.style.display = "block"

		window.onclick = function(event) 
		{
			if (event.target == popup) 
			{
				popup.style.display = "none";
				popup.remove()
			}
		}
	}

	static affichePopupConfirmation(texte, fonction)
	{
		const popup = document.createElement('div')
		popup.classList.add("popup")
		popup.innerHTML = `
		<div class="popupContent">
			<div class="elementPopup titrePopup">
				<p>Confirmation</p>
			</div>
			<div class="elementPopup">
				<p>` + texte + `</p>
			</div>
			<div class="elementPopup">
				<button type="button" class="bouton boutonPopup" id="annulation">Annuler</button>
				<button type="button" class="bouton boutonPopup" id="confirmation">Confirmer</button>
			</div>
		</div>`
		const html = document.querySelector("html")
		html.append(popup)
		popup.style.display = "block"

		window.onclick = function(event) 
		{
			if (event.target == popup) 
			{
				popup.style.display = "none";
				popup.remove()
			}
		}
		
		const boutonAnnulation = document.getElementById("annulation");
		boutonAnnulation.onclick = function()
		{
			popup.style.display = "none"
			popup.remove()
		}

		const boutonConfirmation = document.getElementById("confirmation");
		boutonConfirmation.onclick = function()
		{
			popup.remove()
			fonction()
		}
	}
}